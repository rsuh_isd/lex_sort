#include <iostream>
#include <vector>

using namespace std;

vector<string> alphabet;

const size_t max_string_length=3;

void print(vector<string> v)
{
    using namespace std;
    clog << endl << "starting printing out " <<v.size() << " strings:" << endl;
    for (int i=0; i<v.size(); i++)
        cout << v[i] << endl;

    cout << endl;
}

vector<string> sort_STRYNGS(vector<string> strings, size_t start_with=0/*default: start sorting by first letter*/)
{
    using namespace std;
    clog << "started sorting strings lexicographically" << endl;
    if (strings.size()<=1) {clog << "nothing to sort, return as is" << endl; return strings;}
    if (start_with>=max_string_length) {clog << "cannot sort by this letter" << endl; return strings;}
    vector<size_t> index;
    for (int i=0; i<strings.size(); i++)
        index.push_back(i);
    vector<vector<size_t> > first_letter_groups;
    for (int i=0; i<alphabet.size();i++)
    {
        vector<size_t> dummy;
        dummy.push_back(0);
        dummy.erase(dummy.begin());
        first_letter_groups.push_back(dummy);
    }

    for (int i=0; i<strings.size(); i++)
    {
        for (int j=0; j< alphabet.size(); j++)
        {
            if (strings[i].substr(start_with,1/*length*/)==alphabet[j])
            {
                clog << "string " << strings[i] << " has letter " << alphabet[j]  << " on position " << start_with << endl;
                first_letter_groups[j].push_back(i);
            }
        }
    }

    vector<string> result;

    for (int i=0; i<alphabet.size(); i++)
    {
        //       working with first_letter_groups[i],
        //          i.e. all strings that start with a
        if (first_letter_groups[i].size()>0)
        {
            vector<string> sub;
            for (size_t j=0; j< first_letter_groups[i].size(); j++)
                sub.push_back(strings[first_letter_groups[i][j]]);
            clog << endl << "TAKE" << endl;
            for (size_t j=0; j< sub.size(); j++)
            {
                cout << sub[j] << endl;
            }
            sub=sort_STRYNGS(sub, start_with+1);
            clog << endl << "THANKS" << endl;
            for (size_t j=0; j< sub.size(); j++)
            {
                cout << sub[j] << endl;
            }
            cout << endl;
            for (size_t j=0; j< sub.size(); j++)
            {
                result.push_back(sub[j]);
            }
            //TODO: instead of printing out, call lex_sort recursively for each set, start_with+1 (sort this group by next letter).
            //that is, get sorted vectors of indeces of each group. and then combine them together. and return later.
            //recursion ends when lex_sort gets one string
            //TODO: different string lengths?
            //cout << endl;
        }
    }
    return result;
}

void lex_sort (vector<string> &strings)
{
    using namespace std;
    strings=sort_STRYNGS(strings);
    //TODO: rearrange strings according to indeces.
    //swapping strings all the time would take too much resources, so we do it only now, not in every sort_indeces
    //like, if indeces[0]=5, then it means that sorting figured out that strings[5] is the smallest and should be strings[0] now.
}

int main()
{
    alphabet.push_back("a");
    alphabet.push_back("b");
    alphabet.push_back("c");

    vector<string> strings;
    strings.push_back("bba");
    strings.push_back("aaa");
    strings.push_back("baa");
    strings.push_back("abb");
    strings.push_back("aba");
    strings.push_back("aaa");
    strings.push_back("baa");

    print(strings);

    lex_sort(strings);

    print(strings);
    return 0;
}
